<?php

namespace lukaskae\bountyModul\models;

use Illuminate\Database\Eloquent\Model;

class Modul extends Model
{
    /**
     * Table is lk_module
     *
     * @var string
     */
    protected $table = 'lk_modules';

    /**
     * Displays only Active bounties
     *
     * @param $query
     */
    public function scopeActive($query)
    {
        $query->where("active", "=", "Yes");
    }
}
